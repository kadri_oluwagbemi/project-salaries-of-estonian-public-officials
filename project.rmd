---
title: "Salaries of Estonian public officials"
subtitle: "Ardi Aasmaa, Kadri Oluwagbemi, Madis Harjo"
output:
  pdf_document: default
  pdf: default
---


## Reading data from files and cleaning it

```{r, echo=FALSE, results='hide', message=FALSE, warning=FALSE}
library(readr)
library(dplyr)
library(data.table)
library(ggplot2)

data2015 <- read.csv("data/pohipalk_01.04.2015_riik_21.07.2015.csv", sep = ";", fileEncoding="UTF-8", strip.white=TRUE, na.strings = c(""))

data2016 <- read.csv("data/pohipalk_01.04.2016_riik.csv", sep = ";", fileEncoding="UTF-8", strip.white=TRUE, na.strings = c("")) 
data2017 <- read.csv("data/riik_pohipalk_01.04.2017.csv", sep = ";",  fileEncoding="UTF-8", strip.white=TRUE, na.strings = c("")) 

totalaveragesal <- read.csv("data/Keskmine brutokuupalk, kuud.csv")

headers = c("Institution", "Department","SubDepartment", "Position","Firstname", "Surname","WorkLoad", "Salary")

oldHeaders = c("Asutus","Struktuuriüksus", "Allstruktuuriüksus", "Ametikoht", "Eesnimi"                         ,"Perekonnanimi", "Ametniku.koormus.ametkohal", "Põhipalk")

oldHeadersal = c("X2005","X2006", "X2007", "X2008", "X2009","X2010", "X2011", "X2012", "X2013", "X2014", "X2015", "X2016", "X2017")

newHeadersal = c("2005","2006", "2007", "2008", "2009","2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017")

totalaveragesal <- subset(totalaveragesal, select = -c(X) )
setnames(totalaveragesal, old=oldHeadersal, new= newHeadersal) #Average salaries of state officials from the year 2005 - 2017

data2015 <- subset(data2015, select = -c(X, X.1, X.2, X.3) )
setnames(data2015, old=oldHeaders, new=headers)
setnames(data2016, old=oldHeaders, new=headers)
setnames(data2017, old=oldHeaders, new=headers)

data2015$Salary = as.double(gsub(" ", "", data2015$Salary))
data2015$WorkLoad = as.double(gsub(",", ".", data2015$WorkLoad))
data2015 = data2015[!is.na(data2015$Institution),]

data2016$Salary = gsub(" €| ", "", data2016$Salary)
data2016$Salary = as.double(gsub(",", ".", data2016$Salary))
data2016$WorkLoad = as.double(gsub(",", ".", data2016$WorkLoad))

data2017$Salary = gsub(" €| ", "", data2017$Salary)
data2017$Salary = as.double(gsub(",", ".", data2017$Salary))
data2017$WorkLoad = as.double(gsub(",", ".", data2017$WorkLoad))
```

#How many of them changed their job to another public sector job?

#Government officials who changed their job to another public sector job in 2015:

- From estimates 71 government officials changed their public sector job in the year 2015.

```{r, changejob}
library(gdata)
library(dplyr)
library(knitr)

dataframe <- as.data.frame(data2015)

cleaned <- dataframe[-which(is.na(dataframe$Surname) & is.na(dataframe$Firstname)),]
freq_names <- cleaned[which(duplicated(cleaned[c("Surname","Firstname")])), ]

freq_surna <- freq_names[duplicated(freq_names$Surname), ] 

grouped <- group_by(cleaned, Surname, Firstname) %>% summarise(count=n()) %>% filter(count>1)


freq_nameandsurname <- data.frame()
  for (i in 1:nrow(grouped)){
    fre <- cleaned[cleaned$Firstname ==  grouped[i,]$Firstname & cleaned$Surname == grouped[i,]$Surname,]
    freq_nameandsurname <- rbind(freq_nameandsurname,fre)
    }
sorted <- freq_nameandsurname[order(freq_nameandsurname$Firstname, freq_nameandsurname$Surname), ]

changeofjob <- select(sorted, Firstname, Surname, Institution)

changec <- group_by(changeofjob, Firstname, Surname, Institution) %>% summarise(count=n()) %>% filter(count == 1)

changec #Shows a table of government officials with their firstname and lastname who are registered to a public sector job.

changec15 <- group_by(changec, Firstname, Surname) %>% summarise(count=n()) %>% filter(count > 1)
kable(changec15) #Counts how many times the government official is registed for more than one job.

nrow(changec15) #Show the total number of number of government officials who changed their job.
```

Some of the officials changed both their job, their position and also their department in the public sector, whereas others retained their jobs but changed their position or department in the public sector job. 

# Visualization of government officials who changed their job to another public sector job in 2015:

Below is a visualization of some names of government officials who changed their job from their public sector job to another in the year 2015 and a count of how may public sector jobs they have changed to. All these officials have all changed their position at least once in 2015 from a public sector job to another.

```{r, fig.width=10, height = 30}
library(ggplot2)
Large <-head(changec15,12)
ggplot(Large, aes(Surname)) + geom_bar(aes(fill = count)) +
coord_cartesian() + scale_color_gradient() + theme_bw() + facet_wrap(Firstname ~ Surname, ncol = 4)
```

#Government officials who changed their job position for 2015:

- From estimates 76 government officials changed their job positions for the year 2015.

```{r}
library(knitr)
changeofPositionp <- select(sorted, Firstname, Surname, Position)
changecp <- group_by(changeofPositionp, Firstname, Surname, Position) %>% summarise(count=n()) %>% filter(count == 1)
changecp 

changec15 <- group_by(changecp, Firstname, Surname) %>% summarise(count=n()) %>% filter(count > 1)
kable(changec15)
nrow(changec15)

```

# A Visualization of 12 registered government officials who have changed their job position in 2015:
- Below is a visualization of 12 government officials who changed their job position in 2015 and a display of the position they have changed to. 

- For example a government official with the Firstname 'Alo' and Surname	'Heinsalu' changed his position in the public sector from	'Direktor' to 'direktor-õiguskantsleri nõunik', it is assumed that this official was promoted which led to a change in the position of his job.	

```{r, fig.width=12, height = 30}
library(ggplot2)
changec <- head(changecp,12)

ggplot(changec, aes(x = Firstname, y= Surname)) + geom_hex() +
facet_wrap(~ Position, ncol = 4)
                                                                                
```

#Government officials who changed their job Department for 2015:
- From estimates 79 government officials changed their job Department for the year 2015.

```{r}
library(knitr)
changeofDepartment <- select(sorted, Firstname, Surname, Department)
changedep <- group_by(changeofDepartment, Firstname, Surname, Department) %>% summarise(count=n()) %>% filter(count == 1)
changedep 

changedep15 <- group_by(changedep, Firstname, Surname) %>% summarise(count=n()) %>% filter(count > 1)
kable(changedep15)
nrow(changedep15)
```

# A Visualization of the job department most government officials changed to in 2015:

- The department "menetlusteenistus" has the largest number of government officials job department change. A lot of government officals in 2015 changed to this job department. 

```{r}
library(ggplot2)
changedepi <- group_by(changeofDepartment, Department) %>% summarise(count=n()) %>% filter(count > 1)
changedepi
ggplot(changedepi, aes(count, Department, color= "menetlusteenistus")) + geom_bin2d(binwidth = c(5, 0.5))

```


#How many of the government officials changed their job to another public sector job in 2016?
- From estimate 71 different government officials also in the year 2016 changed their job to another public sector job just like in the year 2015.

```{r, changejob}
library(gdata)
library(dplyr)
library(knitr)

dataframe16 <- as.data.frame(data2016)
cleaned16 <- dataframe16[-which(is.na(dataframe16$Surname) & is.na(dataframe16$Firstname)),]

freq_names16 <- cleaned16[which(duplicated(cleaned16[c("Surname","Firstname")])), ]

freq_surna16 <- freq_names16[duplicated(freq_names16$Surname), ] 

grouped16 <- group_by(cleaned16, Surname, Firstname) %>% summarise(count=n()) %>% filter(count>1)


freq_namensurname <- data.frame()
  for (i in 1:nrow(grouped16)){
    fre16 <- cleaned16[cleaned16$Firstname ==  grouped16[i,]$Firstname & cleaned16$Surname == grouped16[i,]$Surname,]
    freq_namensurname <- rbind(freq_namensurname,fre16)
    }
sorted16 <- freq_namensurname[order(freq_namensurname$Firstname, freq_namensurname$Surname), ]

changeofInstitution16 <- select(sorted16, Firstname, Surname, Institution)
changed16 <- group_by(changeofInstitution16, Firstname, Surname,Institution) %>% summarise(count=n()) %>% filter(count == 1)
changed16

changec16 <- group_by(changed16, Firstname, Surname) %>% summarise(count=n()) %>% filter(count > 1)
kable(changec16) #Counts how many times the government official is registed for more than one job.
nrow(changec16)
 
```

#Visualization of government officials changed their job to another public sector job in 2016:

-This government official with the (Firstname = "Ülle", Surname =	'Laur') changed job more to another public sector job in 2016 compared to the remaining officials.


```{r, fig.width=10, height=30}
library(ggplot2)
f <- ggplot(changec16, aes(count, Firstname))+ geom_tile(aes(fill = Surname))+ facet_grid(. ~ count, scales = "free")+ theme(legend.position = "right") + geom_text(aes(label = Firstname)) + theme_grey()
f

```


#Government officials who changed their job position for 2016:
- From estimates 78 government officials changed their job positions for the year 2016.

```{r}
library(knitr)
changeofPosition16 <- select(sorted16, Firstname, Surname, Position)
changeP <- group_by(changeofPosition16, Firstname, Surname, Position) %>% summarise(count=n()) %>% filter(count == 1)
changeP 

changec16 <- group_by(changeP, Firstname, Surname) %>% summarise(count=n()) %>% filter(count > 1)
kable(changec16)
nrow(changec16)
```

#Government officials who changed their job Department for 2016:
- From estimates 71 government officials changed their job Department for the year 2016.

```{r}
library(knitr)
changeofDepartment16 <- select(sorted16, Firstname, Surname, Department)
changedep16 <- group_by(changeofDepartment16, Firstname, Surname, Department) %>% summarise(count=n()) %>% filter(count == 1)
changedep16 

changedp16 <- group_by(changedep16, Firstname, Surname) %>% summarise(count=n()) %>% filter(count > 1)
kable(changedp16)
nrow(changedp16)
```


#Government officials who changed their job to another public sector job in 2017:
- From estimates 66 government officials their job to another public sector job in 2017. A few reduction in the total amount of government officials who changed in the year 2016.

```{r}
library(gdata)
library(dplyr)
library(knitr)

dataframe17 <- as.data.frame(data2017)

cleaned17 <- dataframe17

freq_names17 <- cleaned17[which(duplicated(cleaned17[c("Surname","Firstname")])), ]

freq_surna17 <- freq_names17[duplicated(freq_names17$Surname), ] 

grouped17 <- group_by(cleaned17, Surname, Firstname) %>% summarise(count=n()) %>% filter(count>1)


freq_namensurname17 <- data.frame()
  for (i in 1:nrow(grouped17)){
    fre17 <- cleaned17[cleaned17$Firstname ==  grouped17[i,]$Firstname & cleaned17$Surname == grouped17[i,]$Surname,]
    freq_namensurname17 <- rbind(freq_namensurname17,fre17)
    }
sorted17 <- freq_namensurname17[order(freq_namensurname17$Firstname, freq_namensurname17$Surname), ]

changeofInstitution17 <- select(sorted17, Firstname, Surname, Institution)
changed17 <- group_by(changeofInstitution17, Firstname, Surname, Institution) %>% summarise(count=n()) %>% filter(count == 1)
changed17

changec17 <- group_by(changed17, Firstname, Surname) %>% summarise(count=n()) %>% filter(count > 1)
kable(changec17) #Counts how many times the government official is registed for more than one job.
nrow(changec17)
```

#Government officials who changed their job Department for 2017:

- From estimates 72 government officials changed their job Department for the year 2017.

```{r}
library(knitr)
changeofPosition17 <- select(sorted17, Firstname, Surname, Position)
changeP17 <- group_by(changeofPosition17, Firstname, Surname, Position) %>% summarise(count=n()) %>% filter(count == 1)
changeP17 

changec17 <- group_by(changeP17, Firstname, Surname) %>% summarise(count=n()) %>% filter(count > 1)
kable(changec17)
nrow(changec17)
```

#Government officials who changed their job Department for 2017:
- From estimates 69 government officials changed their job Department for the year 2017.

```{r}
library(knitr)
changeofDepartment17 <- select(sorted17, Firstname, Surname, Department)
changedep17 <- group_by(changeofDepartment17, Firstname, Surname, Department) %>% summarise(count=n()) %>% filter(count == 1)
changedep17 

changedp17 <- group_by(changedep17, Firstname, Surname) %>% summarise(count=n()) %>% filter(count > 1)
kable(changedp17)
nrow(changedp17)
```



##Have the salaries of official increased or not?

```{r}
summaryByInst2015 = data2015 %>% group_by (Institution) %>% summarise(meanSalary = mean(Salary), medianSalary= median(Salary), minSalary= min(Salary), maxSalary = max(Salary), total = n())

summaryByInst2016 = data2016 %>% group_by (Institution) %>% summarise(meanSalary = mean(Salary), medianSalary= median(Salary), minSalary= min(Salary), maxSalary = max(Salary), total = n())

summaryByInst2017 = data2017 %>% group_by (Institution) %>% summarise(meanSalary = mean(Salary), medianSalary= median(Salary), minSalary= min(Salary), maxSalary = max(Salary), total = n())

summaryByInst2015$Year = "2015"
summaryByInst2016$Year = "2016"
summaryByInst2017$Year = "2017"
summaryByInstAll<-rbind(summaryByInst2015, summaryByInst2016, summaryByInst2017)

institutions = unique(summaryByInstAll$Institution)

calculateDiffs <- function(dataF, attr, firstYear, secondYear, institutionsF, resultInPercents) {
  diffsVector <- c()
  for(institution in institutionsF){
    attrFirst = as.numeric(dataF[dataF$Year == firstYear &
                                            dataF$Institution ==  institution,][attr])
    attrSecond = as.numeric(dataF[dataF$Year == secondYear &
                                            dataF$Institution == institution,][attr])
    if(!is.na(attrFirst) && !is.na(attrSecond)){
      if(isTRUE(resultInPercents)) {
        diffsVector <- append(diffsVector, 1-(attrFirst/attrSecond))  
      } else{
        diffsVector <- append(diffsVector, attrSecond - attrFirst) 
      }
    } else {
      diffsVector <- append(diffsVector, NA)
    }
  }
  return(diffsVector)
}
calculateDiffs
calculateStats <- function(dataF1, institutionsF1, firstYear, secondYear) {
  Institution <- institutionsF1
  MedianSalary1 <- c()
  MedianSalary2 <- c()
  MeanSalary1 <- c()
  MeanSalary2 <- c()
  MinSalary1 <- c()
  MinSalary2 <- c()
  MaxSalary1 <- c()
  MaxSalary2 <- c()
  NrOfEmployees1 <- c()
  NrOfEmployees2 <- c()
  NrOfEmployeesDiff <- c()
  NrOfEmployeesDiffPer <- c()
  SalaryBudget1 <- c()
  SalaryBudget2 <- c()
  SalaryBudgetDiff <- c()
  SalaryBudgetDiffPer <- c()
  MedianSalaryDiff <- calculateDiffs(dataF1, "medianSalary", firstYear, secondYear, institutionsF1, FALSE)
  MedianSalaryDiffPer <- calculateDiffs(dataF1, "medianSalary", firstYear, secondYear, institutionsF1, TRUE)
  MeanSalaryDiff <- calculateDiffs(dataF1, "meanSalary", firstYear, secondYear, institutionsF1, FALSE)
  MeanSalaryDiffPer <- calculateDiffs(dataF1, "meanSalary", firstYear, secondYear, institutionsF1, TRUE)
  MinSalaryDiff <- calculateDiffs(dataF1, "minSalary", firstYear, secondYear, institutionsF1, FALSE)
  MinSalaryDiffPer <- calculateDiffs(dataF1, "minSalary", firstYear, secondYear, institutionsF1, TRUE)
  MaxSalaryDiff <- calculateDiffs(dataF1, "maxSalary", firstYear, secondYear, institutionsF1, FALSE)
  MaxSalaryDiffPer <- calculateDiffs(dataF1, "maxSalary", firstYear, secondYear, institutionsF1, TRUE)
  df = NULL
  for(institutionV in institutionsF1){
    MedianSalary1 = append(MedianSalary1, as.numeric(dataF1[dataF1$Year == firstYear &
                                            dataF1$Institution ==  institutionV,]["medianSalary"]))
    MedianSalary2 = append(MedianSalary2, as.numeric(dataF1[dataF1$Year == secondYear &
                                            dataF1$Institution ==  institutionV,]["medianSalary"]))
    mean1 = as.numeric(dataF1[dataF1$Year == firstYear &
                                            dataF1$Institution ==  institutionV,]["meanSalary"])
    mean2 = as.numeric(dataF1[dataF1$Year == secondYear &
                                            dataF1$Institution ==  institutionV,]["meanSalary"])
    total1 = as.numeric(dataF1[dataF1$Year == firstYear &
                                            dataF1$Institution ==  institutionV,]["total"])
    total2 = as.numeric(dataF1[dataF1$Year == secondYear &
                                            dataF1$Institution ==  institutionV,]["total"])
    MeanSalary1 = append(MeanSalary1, mean1)
    MeanSalary2 = append(MeanSalary2, mean2)
    NrOfEmployees1 = append(NrOfEmployees1, total1)
    NrOfEmployees2 = append(NrOfEmployees2, total2)
    budget1 = NA
    budget2 = NA
    if(!is.na(total1)  && !is.na(total2)){
      NrOfEmployeesDiff = append(NrOfEmployeesDiff, total2 - total1)
      NrOfEmployeesDiffPer = append(NrOfEmployeesDiffPer, 1 - (total1/total2))
    }else{
      NrOfEmployeesDiff = append(NrOfEmployeesDiff, NA)
      NrOfEmployeesDiffPer = append(NrOfEmployeesDiffPer, NA)
    }
    if(!is.na(total1)  && !is.na(mean1)){
      budget1 = total1 * mean1
    }
    if(!is.na(total2)  && !is.na(mean2)){
      budget2 = total2 * mean2
    }
    SalaryBudget1 <- append(SalaryBudget1, budget1)
    SalaryBudget2 <- append(SalaryBudget2, budget2)
    if(!is.na(total1)  && !is.na(mean1)){
      SalaryBudgetDiff <- append(SalaryBudgetDiff, budget2 - budget1)
      SalaryBudgetDiffPer <- append(SalaryBudgetDiffPer, 1 - (budget1/budget2))
    } else{
      SalaryBudgetDiff <- append(SalaryBudgetDiff, NA)
      SalaryBudgetDiffPer <- append(SalaryBudgetDiffPer, NA)
    }
    
    MinSalary1 = append(MinSalary1, as.numeric(dataF1[dataF1$Year == firstYear &
                                            dataF1$Institution ==  institutionV,]["minSalary"]))
    MinSalary2 = append(MinSalary2, as.numeric(dataF1[dataF1$Year == secondYear &
                                            dataF1$Institution ==  institutionV,]["minSalary"]))
    MaxSalary1 = append(MaxSalary1, as.numeric(dataF1[dataF1$Year == firstYear &
                                            dataF1$Institution ==  institutionV,]["maxSalary"]))
    MaxSalary2 = append(MaxSalary2, as.numeric(dataF1[dataF1$Year == secondYear &
                                            dataF1$Institution ==  institutionV,]["maxSalary"]))
  }  
  df = data.frame(Institution, MedianSalary1, MedianSalary2, MedianSalaryDiff, MedianSalaryDiffPer, MeanSalary1, MeanSalary2, MeanSalaryDiff, MeanSalaryDiffPer, MinSalary1, MinSalary2, MinSalaryDiff, MinSalaryDiffPer, MaxSalary1, MaxSalary2, MaxSalaryDiff, MaxSalaryDiffPer, NrOfEmployees1, NrOfEmployees2, NrOfEmployeesDiff, NrOfEmployeesDiffPer, SalaryBudget1, SalaryBudget2, SalaryBudgetDiff, SalaryBudgetDiffPer)
  return(df)
}

# For every department nr of employees, budget size, min-, max-, mean-, median salary for years 2015 and 2016 and diffs (in euros/items and also in percents) for every stat
stats1516 <- calculateStats(summaryByInstAll, institutions, "2015", "2016")
#Same for 2016 2017
stats1617 <- calculateStats(summaryByInstAll, institutions, "2016", "2017")
#write.csv(stats1516, "data/stats1516.csv", fileEncoding="UTF-8") 
#write.csv(stats1617, "data/stats1617.csv", fileEncoding="UTF-8") 

medianChangeDiffs1516 <- calculateDiffs(summaryByInstAll, "medianSalary", "2015", "2016", institutions, FALSE)
medianChangeDiffs1617 <- calculateDiffs(summaryByInstAll, "medianSalary", "2016", "2017", institutions, FALSE)

meanChangeDiffs1516 <- calculateDiffs(summaryByInstAll, "meanSalary", "2015", "2016", institutions, FALSE)
meanChangeDiffs1617 <- calculateDiffs(summaryByInstAll, "meanSalary", "2016", "2017", institutions, FALSE)

minChangeDiffs1516 <- calculateDiffs(summaryByInstAll, "minSalary", "2015", "2016", institutions, FALSE)
minChangeDiffs1617 <- calculateDiffs(summaryByInstAll, "minSalary", "2016", "2017", institutions, FALSE)

maxChangeDiffs1516 <- calculateDiffs(summaryByInstAll, "maxSalary", "2015", "2016", institutions, FALSE)
maxChangeDiffs1617 <- calculateDiffs(summaryByInstAll, "maxSalary", "2016", "2017", institutions, FALSE)

## Conclusion: Median salary of institutions comparing 2015 - 2016
## -1 - salary decreased, 0 - salary is equal, 1 - salary raised, NA - info about one of the years is unknown
table(sign(medianChangeDiffs1516), exclude = NULL)
## Same about 2016 - 2017
table(sign(medianChangeDiffs1617), exclude = NULL)

table(sign(meanChangeDiffs1516), exclude = NULL)
table(sign(meanChangeDiffs1617), exclude = NULL)

table(sign(minChangeDiffs1516), exclude = NULL)
table(sign(minChangeDiffs1617), exclude = NULL)

table(sign(maxChangeDiffs1516), exclude = NULL)
table(sign(maxChangeDiffs1617), exclude = NULL)


```



```{r}
shapes  <- c("s1" = 1, "s2" = 2, "s3"=3)

mean(summaryByInst2015$meanSalary)
mean(summaryByInst2016$meanSalary)
mean(summaryByInst2017$meanSalary)

median(summaryByInst2015$medianSalary)
median(summaryByInst2016$medianSalary)
median(summaryByInst2017$medianSalary)

summaryByInstAll
ggplot(summaryByInstAll, aes(x=summaryByInstAll$Institution, color=summaryByInstAll$Year)) + 
geom_point(aes(y=summaryByInstAll$medianSalary, size=summaryByInstAll$total,  shape="s1")) + 
geom_point(aes(y = summaryByInstAll$minSalary, shape="s2")) +
geom_point(aes(y = summaryByInstAll$maxSalary, shape="s3")) + 
geom_hline(aes(yintercept=median(summaryByInst2015$medianSalary)), color = "red") +
geom_hline(aes(yintercept=median(summaryByInst2016$medianSalary)), color = "green") +
geom_hline(aes(yintercept=median(summaryByInst2017$medianSalary)), color = "blue") +
labs(title="Change in public officials salary 2015-2017", x="Institution", y="Salaries") + scale_color_discrete(name="Year") + 
scale_shape_manual(name = "Salaries", 
                              breaks = c("s1", "s2", "s3"),
                              values = shapes,
                              labels = c("Median", "Min", "Max")) +
scale_size_continuous(name="Employees") + 
scale_y_continuous(minor_breaks = seq(0 , 6000, 250), breaks = seq(0, 6000, 500)) +  
theme(legend.position="bottom", legend.text=element_text(size=8), axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1, size=4))

```


#Predicting the average salaries of workers for the year 2018 based on their registered Institution:

K-fold Cross-validation was used for estimating our model performance and accuracy. From supervised machine learning we chose to use Random forest and Decision tree algorithm to predict our model. 

We splitted all dataset into 70% of training set and 30% of test set. The training set is used for building the model. 

For Random forest algorithm we made use of 10-fold cross validation to estimate the average salary from 2005 - 2017 in order to predict the average salary of government officials for 2018.

```{r, fig.width=10, height=20}
library(caret)
set.seed(101)

data <- as.data.frame (summaryByInstAll)

totalaveragesalsplit <- sample(2, nrow(data), replace = TRUE, prob = c(0.70, 0.30))

totalsaltrain <- data[totalaveragesalsplit== 1,]
totalsaltest <- data[totalaveragesalsplit== 2,]

x_train <- totalsaltrain 
y_train <- totalsaltrain$medianSalary
z_train <- totalsaltrain$Year
x_testavg <- totalsaltest 


x <- cbind(x_train, y_train, z_train)

ctrl <- trainControl(method="cv", number = 10)
rf_fit <- train(y_train ~ z_train, data = x, method = 'rf', trControl = ctrl)
avgSalary2018 <- predict(rf_fit,  newdata = x_testavg)

d <- as.data.frame(avgSalary2018)

predict <- cbind(x_train, avgSalary2018)
predict$avgSalary2018 
```

By using the Random forest algorithm the average salaries of officials in the 2018 for each Institution was predicted to be from 1278.571 to 1574.328.


Using Decision tree algorithm we splitted the data into Repeated k-fold Cross Validation using 5-fold cross validation with 3 repeats to estimate the average Salary from 2005 - 2017 in order to predict the average salary of government officials for 2018.

```{r}
ctrl <- trainControl(method="repeatedcv", number = 5, repeats = 3)
rf_fit <- train(y_train ~ z_train, data = x, method = 'rpart', trControl = ctrl)
AverageSalary2018 <- predict(rf_fit,  newdata = x_testavg)

di <- as.data.frame(AverageSalary2018)

predict2018 <- cbind(x_train, AverageSalary2018)
predict2018$AverageSalary2018

```

By using Decision tree algorithm the mean Salary of officials in the 2018 for each Institution was predicted to be from 1278.298 to 1575.801. Basically the same prediction. 


#Visualization of Average Salary prediction for government officials in 2018:

Below is a visualization of Average Salary prediction for government officials in 2018:


```{r, fig.width=15, height=35}
library(ggplot2)

theme_set(theme_bw())  # pre-set the bw theme.

g <- ggplot(predict2018, aes(Institution, AverageSalary2018, color=Institution))

# Scatterplot
g + geom_boxplot() + 
  geom_smooth(method="lm", se=F) +
  labs(subtitle="Institution vs Average Salary (Estimated Average Salary 2018 from range 1278.298 to 1575.801)", 
       y="AverageSalary2018", 
       x="Institution", 
       title="Average Salary estimate for government officials in 2018", 
       caption="Estimated average Salary 2018 from 1278.298 to 1575.801") +  
theme(legend.position="bottom", legend.text=element_text(size=13), axis.text.x = element_text(angle = 90, vjust = 1, hjust=1, size=1))
```

When we compared the result of our prediction with a true dataset from https://www.stat.ee/stat-keskmine-brutokuupalk about state officials average salaries we can proof that our prediction of state officals average salary for 2018 is 96% to 100% true.

```{r}
library(knitr)

kable(totalaveragesal)
```